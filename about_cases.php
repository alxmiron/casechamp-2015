<!DOCTYPE html>
<html lang="ru">
<head>
	<?php include('head.php'); ?>
	<title>CaseChamp: О бизнес-кейсах</title>
</head>
<body>
	<?php include('header.php'); ?>

	<section class="topSlider tsabout parallax-window" data-parallax="scroll" data-image-src="img/aboutCase.jpg">
		<div class="topMenu">
			<div class="container">
				<ul>
					<li><a href="index.php">О чемпионате</a></li>
					<li><a href="registration.php">Регистрация</a></li>
					<li><a href="corresp_tour.php">Заочный тур</a></li>
					<li><a href="training.php">Тренинги</a></li>
					<li><a href="about_cases.php">О бизнес-кейсах</a></li>
					<li><a href="organizers.php">Организаторы</a></li>
					<li><a href="contacts.php">Контакты</a></li>
				</ul>
			</div>
		</div>
		<div class="pageTitle">
			<h1>О бизнес-кейсах</h1>
		</div>
	</section>
	<section class="block-white caseHistory"><!-- parallax-window" data-parallax="scroll" data-image-src="img/aboutCase2.jpg"> -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Немного истории</h1>
					<div class="underline-main">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Метод бизнес-кейсов впервые был применен в Гарвардской бизнес-школе в 1908 году. Тогда не существовало учебников, подходящих для аспирантской программы в бизнесе. И решение данной проблемы было следующим: слушателям давалось описание определенной ситуации, с которой столкнулась реальная организация в своей деятельности. Студенты знакомились с проблемой и находили решение в ходе коллективного обсуждения. С тех пор кейс-метод широко используется в бизнес-обучении во всем мире и продолжает завоевывать новых сторонников.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="block-yellow whatIsCase">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Что же такое бизнес-кейс?</h1>
					<div class="underline-dark">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Кейс содержит в себе информацию о ситуации в организации: письменное описание ситуации, графики, таблицы и определенную количественную информацию. Некоторые кейсы очень кратки и состоят из нескольких страниц. Другие — более обширны и включают в себя «подлинную» информацию, такую, как выдержки из прессы, копии внутренних распоряжений, отчеты по исследованиям, финансовую отчетность. Иногда предоставляются видео- или аудиоматериалы, которые могут включать в себя интервью с главными действующими лицами компании. Объем некоторых кейсов — 60-80 страниц.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="block-white example2014">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Пример: IASA CaseChamp 2014 (финал)</h1>
					<div class="underline-main">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Участникам чемпионата предлагалось испытать себя в качестве консультантов в создании стратегии для мощного игрока ИТ-индустрии - компании Yahoo, Inc. Они также должны были разработать план действий для совета директоров, которые бы укладывались в новую стратегию и предложить шаги по ee реализации.<br>
					</p>
					<div class="row">
						<div class="col-md-12">
							<div class="linkButton">
								<a href="docs/IASA CaseChamp Final Task.pdf" target="_blank">
									<p>Cкачать задание бизнес-кейса</p>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="block-white exPres2014">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Презентации финалистов прошлого чемпионата</h1>
					<div class="underline-main">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
				</div>

				<div class="pres col-md-3">
					<h3>
						<a href="https://www.slideshare.net/secret/tzqkp4sxaGnbE2" target="_blank">
							<small>Команда</small><br>"Citadel"
						</a>
					</h3>
					<iframe src="//www.slideshare.net/slideshow/embed_code/key/tzqkp4sxaGnbE2" width="100%" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="height:250px" allowfullscreen></iframe>
					<div style="padding:5px 0 12px"></div>
				</div>
				<div class="pres col-md-3">
					<h3>
						<a href="http://www.slideshare.net/secret/3GYjxXbjkBRAYl" target="_blank">
							<small>Команда</small><br>"Protege Project"
						</a>
					</h3>
					<iframe src="//www.slideshare.net/slideshow/embed_code/key/3GYjxXbjkBRAYl" width="100%" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="height:250px" allowfullscreen></iframe>
					<div style="padding:5px 0 12px"></div>
				</div>
				<div class="pres col-md-3">
					<h3>
						<a href="http://www.slideshare.net/secret/MF6XdeeBiKBbF8" target="_blank">
							<small>Команда</small><br>"Superiority"
						</a>
					</h3>
					<iframe src="//www.slideshare.net/slideshow/embed_code/key/MF6XdeeBiKBbF8" width="100%" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="height:250px" allowfullscreen></iframe>
					<div style="padding:5px 0 12px"></div>
				</div>
				<div class="pres col-md-3">
					<h3>
						<a href="http://www.slideshare.net/secret/hkRHi0xXkf5pBf" target="_blank">
							<small>Команда</small><br>"Princeps"
						</a>
					</h3>
					<iframe src="//www.slideshare.net/slideshow/embed_code/key/hkRHi0xXkf5pBf" width="100%" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="height:250px" allowfullscreen></iframe>
					<div style="padding:5px 0 12px"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="block-white howToSolve">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Как решать бизнес-кейс?</h1>
					<div class="underline-main">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Независимо от природы предоставленного вам кейса, в первую очередь необходимо его проанализировать. Выявить, какие процессы протекают, почему они протекают и что может произойти в будущем. Выделить основные проблемы, выделить факторы, которые могут повлиять на их решение. Проработать несколько сценариев развития событий. Проанализировать последствия принятия того или иного решения.<br><br>
						Не забывайте: чтобы быстро и эффективно вылечить больного, необходимо поставить правильный диагноз. Поэтому чаще задавайте себе вопрос «Почему?». И помните, что в этом деле самое главное — не количество знаний, которыми Вы обладаете, а <b>умение быстро и правильно разобраться в ситуации и принять правильное решение</b>.
					</p><br>
					<?php /*
					<div class="row">
						<div class="col-md-12">
							<div class="linkButton">

								<a href="http://college.cengage.com/business/resources/casestudies/students/analyzing.htm" target="_blank">
									<p>Analyzing a case study</p>
								</a>

							</div>
						</div>
					</div>
					*/?>
				</div>
			</div>
		</div>
	</section>

	<section class="block-yellow reasonToSolve">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Зачем решать кейс?</h1>
					<div class="underline-dark">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<div class="col-md-6">
						<p>Решая кейс, Вы сможете развить в себе множество качеств и навыков:<br>
						&bull; умение конструктивно мыслить<br>
						&bull; умение структурировать: отсеивать лишние детали и выделять существенное<br>
						&bull; умение презентовать себя и свой проект<br>
						&bull; умение работать в команде<br>
						</p>
					</div>
					<div class="col-md-6">
						<p>Решение бизнес-кейса — это:<br>
						&bull; отличная возможность попрактиковаться в решении реальных бизнес-задач<br>
						&bull; возможность проверить себя, на что ты способен<br>
						</p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('partners-block.php'); ?>

	<?php include('orgs-block.php'); ?>

	<?php include('footer.php'); ?>
</body>
</html>
