<!DOCTYPE html>
<html lang="ru">
<head>
	<?php include('head.php'); ?>
	<title>CaseChamp: Контакты</title>
	</script>
</head>
<body>
	<?php include('header.php'); ?>

	<section class="topSlider tscont parallax-window" data-parallax="scroll" data-image-src="img/contacts.jpg">
		<div class="topMenu">
			<div class="container">
				<ul>
					<li><a href="index.php">О чемпионате</a></li>
					<li><a href="registration.php">Регистрация</a></li>
					<li><a href="corresp_tour.php">Заочный тур</a></li>
					<li><a href="training.php">Тренинги</a></li>
					<li><a href="about_cases.php">О бизнес-кейсах</a></li>
					<li><a href="organizers.php">Организаторы</a></li>
					<li><a href="contacts.php">Контакты</a></li>
				</ul>
			</div>
		</div>
		<div class="pageTitle">
			<h1>Контакты</h1>
		</div>
	</section>

	<section class="block-white contacts">
		<div class="container">
			<div class="row org1">
				<div class="col-md-2">
					<img src="img/org1.jpg" width="100%">
				</div>

				<div class="col-md-5 contProfile ss">
					<p>
						<span>Юрий Захарян</span><br>
						Email: yuri.zakharyan@gmail.com<br>
						Моб: +38 (063) 599-81-92
						&nbsp;
					</p>
				</div>
			</div>
			<p>С нами всегда можно связаться по электронной почте — <i>studcasechamp@gmail.com</i></p>
			<div class="underline-main">
				<div class="underline-line"></div>
				<div class="underline-square"></div>
			</div>

			<div class="row org2">
				<div class="col-md-2">
					<img src="img/org2.jpg" width="100%">
				</div>

				<div class="col-md-5 contProfile kse">
					<p>
						<span>Татьяна Вознюк</span><br>
						Email: tvozniuk@kse.org.ua<br>
						Моб: +38 (093) 780-81-30
					</p>
				</div>

				<div class="col-md-5 contProfile kse">
					<p>
						<span>Анна Яковенчук</span><br>
						Email: ayakovenchuk@kse.org.ua<br>
						Моб: +38 (093) 561-12-83
					</p>
				</div>
			</div>
			<p id="lastEmail">С нами всегда можно связаться по электронной почте — <i>caseclub@kse.org.ua</i></p>
		</div>
	</section>

	<?php include('footer.php'); ?>
</body>
</html>
