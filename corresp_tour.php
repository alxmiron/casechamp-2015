<!DOCTYPE html>
<html lang="ru">
<head>
	<?php include('head.php'); ?>
	<title>CaseChamp: Заочный тур</title>
</head>
<body>
	<?php include('header.php'); ?>

	<section class="topSlider tscortour parallax-window" data-parallax="scroll" data-image-src="img/correspTour.jpg">
		<div class="topMenu">
			<div class="container">
				<ul>
					<li><a href="index.php">О чемпионате</a></li>
					<li><a href="registration.php">Регистрация</a></li>
					<li><a href="corresp_tour.php">Заочный тур</a></li>
					<li><a href="training.php">Тренинги</a></li>
					<li><a href="about_cases.php">О бизнес-кейсах</a></li>
					<li><a href="organizers.php">Организаторы</a></li>
					<li><a href="contacts.php">Контакты</a></li>
				</ul>
			</div>
		</div>
		<div class="pageTitle">
			<h1>Заочный тур</h1>
		</div>
	</section>


	<section class="block-white correspTour">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Заочный тур</h1>
					<div class="underline-main">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Заочный этап «CaseChamp Student League» будет проходить с <b>27 апреля</b> по <b>7</b> мая. Задание кейса вскоре будет опубликовано на этой странице.
					</p>
					<?php /*
					<div class="row">
						<div class="col-md-12">
							<div class="linkButton">
								<p>Скачать задание кейса</p>
							</div>
						</div>
					</div>
					<p>
						Готовые решения капитаны команд должны прислать на электронную почту организаторов: <i>iasa.casechamp@gmail.com</i>. Дедлайн приема решений — <b>23:59 20-го апреля</b>.
					</p>
					*/?>
				</div>
			</div>
		</div>
	</section>
	<section class="block-yellow howToFormat">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Как оформить решение?</h1>
					<div class="underline-dark">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>Решение кейса следует оформить в виде презентации в формате ppt или pdf. Рекомендуемый объем — до <b>10 слайдов</b>. Презентация, помимо решения, должна включать слайд с представлением команды, а также слайд с рекомендациями.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="block-white howToSolveShort">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Как решать бизнес-кейс?</h1>
					<div class="underline-main">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Чтобы помочь участникам сориентироваться в бизнес-кейсах, а также дать возможность попрактиковаться перед чемпионатом, мы совместно с компаниями-партнерами проводим <a href="training.php">тренинги</a> для всех желающих.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="block-yellow cryteria">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Критерии оценивания</h1>
					<div class="underline-dark">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<div class="row">
						<div class="col-md-4 cryteriaItem">
							<div class="procentBox">
								<h3>Структура решения</h3>
								<p>30%</p>
							</div>
							<p class="text">
								• идентификация поставленных задач и требований<br><br>
								• summary-слайд, подводящий итоги и конкретизирующий выводы<br><br>
								• громоздкие вычисления и таблицы вынесены в конец презентации<br><br>
								• тезисное построение решения<br><br>
							</p>
						</div>
						<div class="col-md-4 cryteriaItem">
							<div class="procentBox">
								<h3>Решение</h3>
								<p>50%</p>
							</div>
							<p class="text">
								• предложенное командой решение полностью отвечает на вопросы, поставленные в условии<br><br>
								• действия логически обоснованны и подкреплены адекватными аргументами<br><br>
								• используются графики и отдельные краткие вычисления с тем, чтобы прояснить и конкретизировать решение<br><br>
								• четкость и однозначность приведенных утверждений<br><br>
								• использование информации, ограниченной к использованию, считается минусом<br><br>
							</p>
						</div>
						<div class="col-md-4 cryteriaItem">
							<div class="procentBox">
								<h3>Оформление</h3>
								<p>20%</p>
							</div>
							<p class="text">
								• читабельный шрифт презентации<br><br>
								• единый стиль текста, таблиц и графиков<br><br>
								• использование в качестве основного одного языка<br><br>
								• согласованная цветовая схема<br><br>
								• визуальное впечатление<br><br>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('partners-block.php'); ?>

	<?php include('orgs-block.php'); ?>

	<?php include('footer.php'); ?>
</body>
</html>
