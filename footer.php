<footer>
  <div class="container">
        <p id="left">Copyright &#169; 2015 All Rights Reserved</p>
        <p id="right">Designed by <a href="https://vk.com/mironenko.alexey" target="_blank">  Alexey Mironenko</a></p>
  </div>
</footer>
<script type="text/javascript" src="libs/jquery-1.11.2.min.js"></script>
<!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<script type="text/javascript" src="libs/parallax.min.js"></script>
<script type="text/javascript" src="libs/flipCountdown/jquery.flipcountdown.js"></script>
<script src="js/common.js"></script>
