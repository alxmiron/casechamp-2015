<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="content-language" content="ru">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="Сайт студенческого чемпионата по решению бизнес-кейсов">
<meta name="keywords" content="Case, Champ, student, league, чемпионат, студенческий, чемп, 2015, решение, кейс, бизнес, решение, команды, тренинг, расписание, Киев, ИПСА, KSE, главная">
<meta name="author" content="Alexey Mironenko">
<link rel="canonical" href="http://studcasechamp.com.ua/index.php">
<link rel="shortcut icon" href="img/favicon.png" type="image/png">
<link rel="icon" href="img/favicon.png" type="image/png">
<link rel="stylesheet" href="css/css/normalize.css">
<link rel="stylesheet" href="libs/bootstrap-grid.min.css">
<link rel="stylesheet" href="fonts/fonts.css">
<link rel="stylesheet" href="css/css/main.css">
<link rel="stylesheet" href="css/css/flow.css">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-62121578-1', 'auto');
  ga('send', 'pageview');
</script>
