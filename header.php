<div id="loader">
  <div id="loaderInner"></div>
</div>
<div class="bigMenu">
  <div class="bigTable">
    <div class="bigDescript">
        <div class="bigWrapper">
          <ul>
            <li><a class="bigMenuItem" href="index.php">О чемпионате</a></li>
            <li><a class="bigMenuItem" href="registration.php">Регистрация</a></li>
            <li><a class="bigMenuItem" href="corresp_tour.php">Заочный тур</a></li>
            <li><a class="bigMenuItem" href="training.php">Тренинги</a></li>
            <li><a class="bigMenuItem" href="about_cases.php">О бизнес-кейсах</a></li>
            <li><a class="bigMenuItem" href="organizers.php">Организаторы</a></li>
            <li><a class="bigMenuItem" href="contacts.php">Контакты</a></li>
          </ul>
        </div>
    </div>
  </div>
</div>
<header class="champName">
  <a href="index.php">
    <div class="logo">
      <img src="img/CaseIcon.png" width="100%">
    </div>
  </a>
  <h1><a href="index.php">CASECHAMP<span>: STUDENT LEAGUE</span></a></h1>
</header>
<a class="toggleMenu">
  <div id="sandwich">
    <div class="sw-topper"></div>
    <div class="sw-bottom"></div>
    <div class="sw-footer"></div>
  </div>
</a>
