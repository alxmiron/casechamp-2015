<!DOCTYPE html>
<html lang="ru">
<head>
	<?php include('head.php'); ?>
	<title>CaseChamp: Student League</title>
</head>
<body>
	<?php include('header.php'); ?>

	<section class="topSlider parallax-window" data-parallax="scroll" data-image-src="img/Digital-Skills.jpg">
	  <div class="topMenu">
	    <div class="container">
	      <ul>
	        <li><a href="index.php">О чемпионате</a></li>
	        <li><a href="registration.php">Регистрация</a></li>
	        <li><a href="corresp_tour.php">Заочный тур</a></li>
	        <li><a href="training.php">Тренинги</a></li>
	        <li><a href="about_cases.php">О бизнес-кейсах</a></li>
	        <li><a href="organizers.php">Организаторы</a></li>
	        <li><a href="contacts.php">Контакты</a></li>
	      </ul>
	    </div>
	  </div>
		<div class="countDown">
			<br><h1>Регистрация на чемпионат открыта!</h1><br>
		</div>
	</section>

	<section class="block-white welcome">
		<!-- parallax-window" data-parallax="scroll" data-image-src="img/aboutCase2.jpg"> -->
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Welcome!</h1>
					<div class="underline-main">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						С радостью хотим представить «Case Champ: Student League».<br><br>
						Этот чемпионат – прекрасная возможность испытать себя в решении реальных проблем и задач ведущих компаний Украины. Кто знает,  возможно именно ты станешь ведущим бизнес-аналитиком страны.<br>
						Ты сможешь узнать много нового, решая практические задачи, а не читая тонны книг и конспектов. Также получишь опыт работы в команде, опыт презентации своих идей и решений. Твою работу будут оценивать настоящие специалисты своих отраслей.<br><br>
						Готов сразиться за звание лучшего? Вперед!
				</div>
			</div>
		</div>
	</section>
	<section class="block-yellow whatIsNeed">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Что нужно, чтобы принять участие в чемпионате?</h1>
					<div class="underline-dark">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Собери команду 3-5 человек<br>
						Зарегистрируй ее на нашем сайте<br>
						Дождись письма-подтверждения об успешной регистрации.<br>
						Регистрация команд продлится до 27-го апреля. Начиная с 27-го апреля зарегистрированные команды начнут получать задание бизнес-кейса.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="block-white howToSolveShort">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Как решать бизнес-кейс?</h1>
					<div class="underline-main">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Твоей команде предстоит разобраться в деятельности некоторой компании, ознакомиться с поставленной проблемой или задачей, которая стоит перед руководством компании (или же догадаться самим, исходя из содержания кейса), и предложить свое решение.<br><br>
						Чтобы помочь участникам сориентироваться в бизнес-кейсах, а также дать возможность попрактиковаться перед чемпионатом, мы совместно с компаниями-партнерами проводим <a href="training.php">тренинги</a> для всех желающих.
					</p>
				</div>
			</div>
		</div>
	</section>
	<section class="block-yellow structure">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Какова структура чемпионата?</h1>
					<div class="underline-dark">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Чемпионат по решению бизнес-кейсов «Case Champ: Student League» проводится в <b>два этапа</b>.<br><br>
						На первом этапе командам необходимо предоставить решение предложенного бизнес-кейса. Пятерка команд, чьи ответы будут оценены членами жюри наивысшими баллами, будет приглашена на второй этап — для <b>полноценной, живой презентации</b> своего решения перед аудиторией — представителями компаний-организаторов и другими командами.<br><br>
						«Очный» этап будет проведен <b>22-го мая</b>. Среди пятерки лучших и будет определен окончательный победитель.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section class="schedule parallax-window" data-parallax="scroll" data-image-src="img/schedule-img.jpg">
		<div class="container">
			<h1>Расписание чемпионата</h1>
			<div class="underline-main">
				<div class="underline-line"></div>
				<div class="underline-square"></div>
			</div>
			<div class="scheduleItemBox">
				<div class="rotsqBox1">
					<div class="rotsq r1"></div>
					<div class="rotsq r3"></div>
					<div class="rotsq r5"></div>
					<div class="rotsq r7"></div>
				</div>
				<div class="rotsqBox2">
					<div class="rotsq r2"></div>
					<div class="rotsq r4"></div>
					<div class="rotsq r6"></div>
				</div>
				<div class="scheduleItem i1">
					<h4>18 апреля</h4>
					<p>Начало регистрации</p>
				</div>
				<div class="scheduleItem i2">
					<h4>21 апреля</h4>
					<p>Первый тренинг</p>
				</div>
				<div class="scheduleItem i3">
					<h4>27 апреля</h4>
					<p>Закрытие регистрации. Начало отборочного тура</p>
				</div>
				<div class="scheduleItem i4">
					<h4>7 мая</h4>
					<p>Окончание отборочного тура</p>
				</div>
				<div class="scheduleItem i5">
					<h4>16 мая</h4>
					<p>Результаты первого тура. Начало очного тура</p>
				</div>
				<div class="scheduleItem i6">
					<h4>19 мая</h4>
					<p>Второй тренинг</p>
				</div>
				<div class="scheduleItem i7">
					<h4>22 мая</h4>
					<p>Финал</p>
				</div>
			</div>
			<div class="timeLineBox">
				<div class="timeLine">
					<div class="timePoint n1"></div>
					<div class="timePoint n2"></div>
					<div class="timePoint n3"></div>
					<div class="timePoint n4"></div>
					<div class="timePoint n5"></div>
					<div class="timePoint n6"></div>
					<div class="timePoint n7"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="block-yellow howToFormat">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Как оформить свое решение?</h1>
					<div class="underline-dark">
						<div class="underline-line"></div>
						<div class="underline-square"></div>
					</div>
					<p>
						Свое решение необходимо будет отправить до 7-го мая включительно на e-mail организаторов чемпионата — <i>studcasechamp@gmail.com</i>.
						Решение должно быть оформлено в виде <b>презентации</b> в формате ppt или pdf и содержать ответ на задание кейса. Рекомендованный объем решения — не более <b>10 слайдов</b>.<br><br>
						Примеры бизнес-кейсов, а также рекомендации по их решению — в разделе <a href="about_cases.php">о бизнес-кейсах</a>
					</p>
				</div>
			</div>
		</div>
	</section>

	<?php include('partners-block.php'); ?>

	<?php include('orgs-block.php'); ?>

	<?php include('footer.php'); ?>
</body>
</html>
