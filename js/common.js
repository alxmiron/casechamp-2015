$(document).ready(function() {

	function heightDetect() {
		$(".topSlider").css("height", $(window).height());
		$(".bigMenu").css("height", $(window).height());
	};
	heightDetect();
	$(window).resize(function() {
		heightDetect();	
	});

	$("#sandwich, .bigMenuItem").click(function() {
 		$("#sandwich").toggleClass("active");
	});

	$(".toggleMenu").click(function(){
		if ($(".bigMenu").is(":visible")) {
			$(".bigMenu").fadeOut(600);
			$(".bigMenu ul li a").removeClass("fadeInUp animated");
		} else {
			$(".bigMenu").fadeIn(600);
			$(".bigMenu ul li a").addClass("fadeInUp animated");
		}
	});

	$(window).scroll(function(){
		if ( $(window).scrollTop() == 0 ){
			$('.champName').css('box-shadow','none');
		}
		if ( $(window).scrollTop() != 0 ){
			$('.champName').css('box-shadow','0 0 20px #6d6d6d');
		}
	});	
	
	$(".bigMenuItem").click(function(){
		$(".bigMenu").fadeOut(600);
	});

	jQuery('#flipcountdownbox1').flipcountdown({
		size:"lg",
		beforeDateTime:'04/24/2015 23:59:59'
	});

	$("#entry_756588376").focus(function(){
		$("#teamNameGuide").css("opacity", 1);
	}).blur(function(){
		$("#teamNameGuide").css("opacity", 0);
	});
	$("#entry_164993824").focus(function(){
		$("#member1EmailGuide").css("opacity", 1);
	}).blur(function(){
		$("#member1EmailGuide").css("opacity", 0);
	});

	var i=4;
	var pname_base=".num";
	var pname="";
	$("#addPerson").click(function(){
		pname=pname_base+i;
		$(pname).fadeIn(600);
		i=i+1;
		if (i>5) {
			$("#addPerson").fadeOut(600);
		}
	});		
});


$(window).load(function() { 
		$("#loaderInner").fadeOut("slow"); 
		$("#loader").delay(400).fadeOut("slow"); 
	});