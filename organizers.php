<!DOCTYPE html>
<html lang="ru">
<head>
	<?php include('head.php'); ?>
	<title>CaseChamp: Организаторы</title>
</head>
<body>
	<?php include('header.php'); ?>

	<section class="topSlider tsorg parallax-window" data-parallax="scroll" data-image-src="img/org.jpg">
		<div class="topMenu">
			<div class="container">
				<ul>
					<li><a href="index.php">О чемпионате</a></li>
					<li><a href="registration.php">Регистрация</a></li>
					<li><a href="corresp_tour.php">Заочный тур</a></li>
					<li><a href="training.php">Тренинги</a></li>
					<li><a href="about_cases.php">О бизнес-кейсах</a></li>
					<li><a href="organizers.php">Организаторы</a></li>
					<li><a href="contacts.php">Контакты</a></li>
				</ul>
			</div>
		</div>
		<div class="pageTitle">
			<h1 style="letter-spacing: 0px;">Организаторы чемпионата</h1>
		</div>
	</section>

	<section class="block-white org">
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<img src="img/org1.jpg" width="100%">
				</div>
				<div class="col-md-10">
					<h1>Студенческий Совет УНК "ИПСА"</h1>
					<p>
						<br>
						Студенческий Совет Учебно-научного комплекса «Институт прикладного системного анализа» — орган студенческого самоуправления, который объединяет всех активных студентов Института.<br><br>
						Основные задания Студенческого Совета — содействие учебной и творческой деятельности студентов, представление интересов студентов УНК «ИПСА», налаживание связей между студентами и администрацией Института, содействие сотрудничеству студенческих организаций в области образования, науки, культуры и спорта, организация досуга и отдыха студентов, а также содействие в трудоустройстве и поиске мест стажировки для студентов.
					</p>
					<div class="vkButton">
						<a href="http://vk.com/studrada_iasa" target="_blank"><img src="img/vk1.png" width="100%"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="block-white org">
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<img src="img/org2.jpg" width="100%">
				</div>
				<div class="col-md-10">
					<h1 id="kse">KSE Case Club</h1>
					<p>
						<br>
						Кейс Клуб Киевской Школы Экономики - это студенческая организация, целью которой является развитие кейс-культуры в Украине. Для тренингов и лекций привлекаются специалисты из отрасли консалтинга, финансов, бизнеса и международных организаций, а участники задействованы в интерактивных лекциях и практических сессиях по решению индивидуальных и командных бизнес-кейсов.
					</p>
					<div class="vkButton">
						<a href="http://vk.com/case_kse" target="_blank"><img src="img/vk2.png" width="100%"></a>
					</div>
					<div class="vkButton">
						<a href="https://www.facebook.com/groups/casekse" target="_blank"><img src="img/fb2.png" width="100%"></a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('footer.php'); ?>
</body>
</html>
