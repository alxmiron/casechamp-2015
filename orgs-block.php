<section class="block-white orgs">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <a href="organizers.php"><h1>Организаторы</h1></a>
        <div class="underline-main">
          <div class="underline-line"></div>
          <div class="underline-square"></div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <a href="organizers.php">
          <div class="row orgrow iasa">
            <div class="col-md-9">
              <h3>Студенческий Совет<br>УНК "ИПСА"</h3>
            </div>
            <div class="col-md-3 orgLogoCont">
              <img src="img/logo_ss.png" width="100%">
            </div>
          </div>
        </a>
      </div>
      <div class="col-md-6">
        <a href="organizers.php">
          <div class="row orgrow kse">
            <div class="col-md-3 orgLogoCont">
              <img src="img/logoKSE.png" width="100%">
            </div>
            <div class="col-md-9">
              <h3>Kyiv School of Economics<br>Case Club</h3>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>
</section>
