<section class="block-white partners">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Партнеры чемпионата</h1>
        <div class="underline-main">
          <div class="underline-line"></div>
          <div class="underline-square"></div>
        </div>
        <div class="row">
          <div class="col-md-8 col-md-push-2">
            <div class="row">
              <div class="col-md-6 partnerLogo">
                <a href="http://www.kpmg.com/ua/en/pages/default.aspx" target="_blank">
                  <img src="img/prt_kpmg_2.png" width="100%" alt="KPMG">
                </a>
              </div>
              <div class="col-md-6 partnerLogo">
                <a href="http://www.kse.org.ua/" target="_blank">
                  <img src="img/prt_kse_2.png" width="100%" alt="KSE">
                </a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 partnerLogo">
                <a href="http://www.ey.com/UA/uk/Home" target="_blank">
                  <img src="img/prt_ey_logo.png" width="100%" alt="EY">
                </a>
              </div>
              <div class="col-md-6 partnerLogo">
                <a href="http://www.pwc.com/ua/en.html" target="_blank">
                  <img src="img/prt_pwc_logo.png" width="100%" alt="EY' Marketing">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
