<!DOCTYPE html>
<html>
<head>
	<?php include('head.php'); ?>
	<title>Заявка принята</title>
</head>
<body>
	<?php include('header.php'); ?>

	<section class="topSlider parallax-window" data-parallax="scroll" data-image-src="img/training.jpg">
		<div class="topMenu">
			<div class="container">
				<ul>
					<li><a href="index.php">О чемпионате</a></li>
					<li><a href="registration.php">Регистрация</a></li>
					<li><a href="corresp_tour.php">Заочный тур</a></li>
					<li><a href="training.php">Тренинги</a></li>
					<li><a href="about_cases.php">О бизнес-кейсах</a></li>
					<li><a href="organizers.php">Организаторы</a></li>
					<li><a href="contacts.php">Контакты</a></li>
				</ul>
			</div>
		</div>
		<div class="pageTitle">
			<h1>Ваша заявка успешно принята!</h1>
		</div>
	</section>

	<script type="text/javascript" src="libs/jquery-1.11.2.min.js"></script>
	<!--[if lt IE 9]>
	    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<script type="text/javascript" src="libs/parallax.min.js"></script>
	<script type="text/javascript" src="libs/flipCountdown/jquery.flipcountdown.js"></script>
	<script src="js/common.js"></script>

</body>
</html>
