<!DOCTYPE html>
<html>
<head>
	<?php include('head.php'); ?>
	<title>CaseChamp: Тренинги</title>
</head>
<body>
	<?php include('header.php'); ?>

	<section class="topSlider parallax-window" data-parallax="scroll" data-image-src="img/training.jpg">
		<div class="topMenu">
			<div class="container">
				<ul>
					<li><a href="index.php">О чемпионате</a></li>
					<li><a href="registration.php">Регистрация</a></li>
					<li><a href="corresp_tour.php">Заочный тур</a></li>
					<li><a href="training.php">Тренинги</a></li>
					<li><a href="about_cases.php">О бизнес-кейсах</a></li>
					<li><a href="organizers.php">Организаторы</a></li>
					<li><a href="contacts.php">Контакты</a></li>
				</ul>
			</div>
		</div>
		<div class="pageTitle">
			<h1>Тренинги</h1>
		</div>
	</section>

	<section class="block-white">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p>
						19-го мая в 19:00 в аудитории 001 35-го корпуса ИПСА (просп. Победы, 37б) пройдет тренинг, посвященный искусству презентации.<br>
						Тренинг будет проводиться представителями EY.
					</p>
					<p>
						Для посещения тренинга вам необходимо заполнить регистрационную форму.<br>
						Регистрация проходит до 19-го мая до 12:00.<br>
						Поля, обозначенные *, обязательны для заполнения.
					</p>
				</div>
			</div>
		</div>
	</section>


	<section class="block-white registration">
		<div class="container">
			<div class="row">
				<div class="col-md-12 profile">
					<h3>Регистрация на тренинг</h3>
					<script type="text/javascript">
						var submitted = false;
					</script>
					<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(submitted) {window.location='http://studcasechamp.com.ua/received.php';}"></iframe>
					<form action="https://docs.google.com/forms/d/1k0mwSFJJG8Y91d_n31b2Bpndi4zwwWqTGOnc5bqz3TM/formResponse" method="POST" id="ss-form" autocomplete="off" target="hidden_iframe" onsubmit="submitted=true;"><ol role="list" class="ss-question-list" style="padding-left: 0">

					<div class="ss-form-question errorbox-good" role="listitem">
					<div dir="ltr" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
					<label class="ss-q-item-label" for="entry_1274933776"><div class="ss-q-title">
					<label for="itemView.getDomIdToLabel()" aria-label="Обязательное поле"></label>
					<span class="ss-required-asterisk" aria-hidden="true"></span></div>
					<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>
					<input type="text" name="entry.1274933776" value="" class="ss-q-short" id="entry_1274933776" dir="auto" aria-label="Фамилия  " placeholder="Фамилия*" aria-required="true" required="" title="">
					<div class="error-message" id="1957389297_errorMessage"></div>
					</div></div></div>

					<div class="ss-form-question errorbox-good" role="listitem">
					<div dir="ltr" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
					<label class="ss-q-item-label" for="entry_106379931"><div class="ss-q-title">
					<label for="itemView.getDomIdToLabel()" aria-label="Обязательное поле"></label>
					<span class="ss-required-asterisk" aria-hidden="true"></span></div>
					<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>
					<input type="text" name="entry.106379931" value="" class="ss-q-short" id="entry_106379931" dir="auto" aria-label="Имя  " placeholder="Имя*" aria-required="true" required="" title="">
					<div class="error-message" id="1189152796_errorMessage"></div>
					</div></div></div>

					<div class="ss-form-question errorbox-good" role="listitem">
					<div dir="ltr" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
					<label class="ss-q-item-label" for="entry_1579012391"><div class="ss-q-title">
					<label for="itemView.getDomIdToLabel()" aria-label="Обязательное поле"></label>
					<span class="ss-required-asterisk" aria-hidden="true"></span></div>
					<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>
					<input type="text" name="entry.1579012391" value="" class="ss-q-short" id="entry_1579012391" dir="auto" aria-label="ВУЗ/Факультет  " placeholder="ВУЗ/Факультет*" aria-required="true" required="" title="">
					<div class="error-message" id="1621114621_errorMessage"></div>
					</div></div></div>

					<div class="ss-form-question errorbox-good" role="listitem">
					<div dir="ltr" class="ss-item ss-item-required ss-radio"><div class="courseSel ss-form-entry">
					<label class="ss-q-item-label" for="entry_559361248"><div class="courseTitle ss-q-title">Курс*
					<label for="itemView.getDomIdToLabel()" aria-label="Обязательное поле"></label>
					<span class="ss-required-asterisk" aria-hidden="true"></span></div>
					<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>

					<ul class="ss-choices" role="radiogroup" aria-label="Курс  "><li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.733625800" value="1" id="group_733625800_1" role="radio" class="ss-q-radio" aria-label="1" required="" aria-required="true"></span>
					<span class="ss-choice-label">1</span>
					</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.733625800" value="2" id="group_733625800_2" role="radio" class="ss-q-radio" aria-label="2" required="" aria-required="true"></span>
					<span class="ss-choice-label">2</span>
					</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.733625800" value="3" id="group_733625800_3" role="radio" class="ss-q-radio" aria-label="3" required="" aria-required="true"></span>
					<span class="ss-choice-label">3</span>
					</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.733625800" value="4" id="group_733625800_4" role="radio" class="ss-q-radio" aria-label="4" required="" aria-required="true"></span>
					<span class="ss-choice-label">4</span>
					</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.733625800" value="5" id="group_733625800_5" role="radio" class="ss-q-radio" aria-label="5" required="" aria-required="true"></span>
					<span class="ss-choice-label">5</span>
					</label></li> <li class="ss-choice-item"><label><span class="ss-choice-item-control goog-inline-block"><input type="radio" name="entry.733625800" value="6" id="group_733625800_6" role="radio" class="ss-q-radio" aria-label="6" required="" aria-required="true"></span>
					<span class="ss-choice-label">6</span>
					</label></li></ul>
					<div class="error-message" id="559361248_errorMessage"></div>
					</div></div></div>

					<div class="ss-form-question errorbox-good" role="listitem">
					<div dir="ltr" class="ss-item ss-item-required ss-text"><div class="ss-form-entry">
					<label class="ss-q-item-label" for="entry_378862843"><div class="ss-q-title">
					<label for="itemView.getDomIdToLabel()" aria-label="Обязательное поле"></label>
					<span class="ss-required-asterisk" aria-hidden="true"></span></div>
					<div class="ss-q-help ss-secondary-text" dir="ltr"></div></label>
					<input type="text" name="entry.378862843" value="" class="ss-q-short" id="entry_378862843" dir="auto" aria-label="Номер телефона  " placeholder="Номер телефона*" aria-required="true" required="" title="">
					<div class="error-message" id="1722748091_errorMessage"></div>

					</div></div></div>
					<input type="hidden" name="draftResponse" value="[,,&quot;-8014780210275572592&quot;]
					">
					<input type="hidden" name="pageHistory" value="0">


					<input type="hidden" name="fbzx" value="-8014780210275572592">

					<div class="ss-item ss-navigate">
						<table class="submit" id="navigation-table"><tbody><tr>
							<td class="ss-form-entry goog-inline-block" id="navigation-buttons" dir="ltr">
								<input class="submitData" type="submit" name="submit" value="Зарегистрироваться" id="ss-submit" class="jfk-button jfk-button-action ">
							</td>
						</tr></tbody></table>
					</div></ol></form>
				</div>
			</div>
		</div>
	</section>

	<?php include('partners-block.php'); ?>

	<?php include('orgs-block.php'); ?>

	<?php include('footer.php'); ?>
</body>
</html>
